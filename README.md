To run the project please follow these steps:
Step 1: Open the CTC.html.
Step 2: Click on Submit to open online OCR. Either upload the test image file or your own file. Copy the extracted text and if required make the correction.
Step 3: Get back to previous site and now click CTC. This will take you to codechef ide where you can paste your copied code and compile it.